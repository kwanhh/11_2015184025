#pragma once
#include "Renderer.h"
#include "Object.h"
#include "Globals.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void Update(float elapsedInsec);
	void RenderScene();
	void DoGarbageCollection();

	int AddObject(float x, float y, float z, 
		float sx, float sy, float sz, 
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass,
		float fricCoef,
		float hp,
		int type);

	void DeleteObject(int idx);

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);

private:
	Renderer* m_Renderer = NULL;
	Sound* m_sound = NULL;
	Object* m_obj[MAX_OBJ_NUM];
	Physics* m_physics = NULL;

	bool m_KeyW = false;
	bool m_KeyA = false;
	bool m_KeyS = false;
	bool m_KeyD = false;
	bool m_KeySP = false;

	bool m_KeyUp = false;
	bool m_KeyDown = false;
	bool m_KeyLeft = false;
	bool m_KeyRight = false;
};

