#pragma once
class Object
{
public:
	Object();
	~Object();

	void Update(float elapsedInsec);
	void AddForce(float x, float y, float z, float elapsedTime);
	void InitPhysics();

	void SetSize(float sx, float sy, float sz);
	void GetSize(float* sx, float* sy, float* sz);
	void SetLocation(float x, float y, float z);
	void GetLocation(float* x, float* y, float* z);
	void SetColor(float r, float g, float b, float a);
	void GetColor(float* r, float* g, float* b, float* a);
	void SetPosition(float x, float y, float z);
	void GetPosition(float* x, float* y, float* z);
	void SetMass(float mass);
	void GetMass(float* mass);
	void SetVelocity(float x, float y, float z);
	void GetVelocity(float* x, float* y, float* z);
	void SetAcceleration(float x, float y, float z);
	void GetAcceleration(float* x, float* y, float* z);
	void SetVolume(float x, float y, float z);
	void GetVolume(float* x, float* y, float* z);
	void SetFricCoef(float coef);
	void GetFricCoef(float* coef);
	void SetType(int type);
	void GetType(int* type);
	void SetTexture(int id);
	void GetTexture(int* id);
	void SetParentObj(Object* parent);
	void GetHP(float* hp);
	void SetHp(float hp);
	void Damage(float damage);
	Object* GetParentObj();
	bool IsAncestor(Object* obj);

	bool CanShootBullet();
	void ResetBulletCoolTime();

private:
	float m_x, m_y, m_z;            //location
	float m_sx, m_sy, m_sz;         //size
	float m_r, m_g, m_b, m_a;       //color
	float m_posX, m_posY, m_posZ;   //position
	float m_mass;                   //mass
	float m_velX, m_velY, m_velZ;   //velocity
	float m_accX, m_accY, m_accZ;   //acceleration
	float m_volX, m_volY, m_volZ;   //volume
	float m_fricCoef;				//friction
	int m_type;
	float m_healthPoint;

	float m_remainingCoolTime = 0.f;
	float m_currentCoolTime = 0.1f;

	int m_texID;
	Object* m_parent = NULL;

};