#pragma once

constexpr auto MAX_OBJ_NUM = 1000;
constexpr auto MAX_CHAR = 100;

constexpr auto HERO_ID = 0;

constexpr auto GRAVITY = 9.8f;

constexpr auto TYPE_NORMAL = 0;
constexpr auto TYPE_BULLET = 1;