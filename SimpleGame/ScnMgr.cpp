#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include "Physics.h"
#include "Renderer.h"

int g_testTexture = -1;
int g_testAnimTex = -1;
int g_enemyTexture = -1;
int g_testBGTexture = -1;
int g_particleTexture = -1;

int g_BGM = -1;
int g_FIRE = -1;
int g_EXPL = -1;

int g_particle = -1;

ScnMgr::ScnMgr()
{
	// Initialize Renderer
	m_Renderer = new Renderer(500, 500);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	//Initialize objects
	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		m_obj[i] = NULL;
	}

	//Init Sound
	//m_sound = new Sound();

	//Create Hero
	m_obj[HERO_ID] = new Object();
	m_obj[HERO_ID]->SetPosition(0, 0, 0);
	m_obj[HERO_ID]->SetVolume(0.5, 0.5, 0.5);
	m_obj[HERO_ID]->SetColor(1, 1, 1, 1);
	m_obj[HERO_ID]->SetVelocity(0, 0, 0);
	m_obj[HERO_ID]->SetMass(1);
	m_obj[HERO_ID]->SetFricCoef(0.7f);
	m_obj[HERO_ID]->SetHp(1000.0f);
	m_obj[HERO_ID]->SetType(TYPE_NORMAL);

	g_testTexture = m_Renderer->GenPngTexture("./Texture/images.png");
	g_enemyTexture = m_Renderer->GenPngTexture("./Texture/enemy.png");
	g_testBGTexture = m_Renderer->GenPngTexture("./Texture/BGImage.png");
	g_particleTexture = m_Renderer->GenPngTexture("./Texture/particle.png");
	m_obj[HERO_ID]->SetTexture(g_testTexture);

	int temp1 = AddObject(
		1, 1, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		0, 0, 0,
		0.5,
		0.7,
		20.0f,
		TYPE_NORMAL);

	int temp2 = AddObject(
		-1, 1, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		0, 0, 0,
		0.5,
		0.7,
		20.0f,
		TYPE_NORMAL);

	int temp3 = AddObject(
		1, -1, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		0, 0, 0,
		0.5,
		0.7,
		20.0f,
		TYPE_NORMAL);

	int temp4 = AddObject(
		-1, -1, 0,
		0.5, 0.5, 0.5,
		1, 1, 1, 1,
		0, 0, 0,
		0.5,
		0.7,
		20.0f,
		TYPE_NORMAL);

	m_obj[temp1]->SetTexture(g_enemyTexture);
	m_obj[temp2]->SetTexture(g_enemyTexture);
	m_obj[temp3]->SetTexture(g_enemyTexture);
	m_obj[temp4]->SetTexture(g_enemyTexture);

	//g_BGM = m_sound->CreateBGSound("./Sounds/bgm.mp3");
	//m_sound->PlayBGSound(g_BGM, true, 1.f);
	//g_FIRE = m_sound->CreateShortSound("./Sounds/fire.mp3");
	//g_EXPL = m_sound->CreateShortSound("./Sounds/explosion.mp3");

	g_particle = m_Renderer->CreateParticleObject(
		200,
		-500, -500,
		500, 500,
		3, 3,
		6, 6,
		-20, -20,
		20, 20);
}

ScnMgr::~ScnMgr()
{
	delete m_Renderer;
	m_Renderer = NULL;
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	//draw background first
	m_Renderer->DrawGround(
		0.0f, 0.0f, 0.0f,
		1000.f, 1000.f, 0.0f,
		1, 1, 1, 1,
		g_testBGTexture,
		1.0f);

	static float pTime = 0.f;
	pTime += 0.016;

	m_Renderer->DrawParticle(
		g_particle,
		0, 0, 0,
		1,
		1, 1, 1, 1,
		0, 0,
		g_particleTexture,
		1, pTime);

	// Draw all m_objs
	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		if (m_obj[i] != NULL)
		{
			float x, y, z = 0;
			float sx, sy, sz = 0;
			float r, g, b, a = 0;
			int texID;

			m_obj[i]->GetPosition(&x, &y, &z);
			m_obj[i]->GetVolume(&sx, &sy, &sz);
			m_obj[i]->GetColor(&r, &g, &b, &a);

			// 1 meter == 100 cm == 100 pixels
			x = x * 100.f;
			y = y * 100.f;
			z = z * 100.f;
			sx = sx * 100.f;
			sy = sy * 100.f;
			sz = sz * 100.f;
			m_obj[i]->GetTexture(&texID);

			//m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);

			m_Renderer->DrawSolidRectGauge(
				x, y, z,
				0, sy / 2.f, 0,
				sx, 5, 0,
				1, 0, 0, 1,
				100.f);

			m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID);
		}
	}
}

void ScnMgr::DoGarbageCollection()
{
	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		if (m_obj[i] == NULL)
			continue;

		int type = -1;
		m_obj[i]->GetType(&type);

		if (type == TYPE_BULLET)
		{
			//Check velocity size
			float vx, vy, vz;

			m_obj[i]->GetVelocity(&vx, &vy, &vz);
			float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
			
			if (vSize < FLT_EPSILON)
			{
				DeleteObject(i);
				continue;
			}
		}
		
		float hp;

		m_obj[i]->GetHP(&hp);

		if (hp < FLT_EPSILON)
		{
			DeleteObject(i);
			continue;
		}
	}
}

int ScnMgr::AddObject(float x, float y, float z,
					  float sx, float sy, float sz,
					  float r, float g, float b, float a,
					  float vx, float vy, float vz,
					  float mass,
					  float fricCoef,
					  float hp,
					  int type)
{
	int idx = -1;

	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		if (m_obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
	{
		std::cout << "No more empty obj slot." << std::endl;
		return -1;
	}

	m_obj[idx] = new Object();
	m_obj[idx]->SetPosition(x, y, z);
	m_obj[idx]->SetVolume(sx, sy, sz);
	m_obj[idx]->SetColor(r, g, b, a);
	m_obj[idx]->SetVelocity(vx, vy, vz);
	m_obj[idx]->SetMass(mass);
	m_obj[idx]->SetFricCoef(fricCoef);
	m_obj[idx]->SetHp(hp);
	m_obj[idx]->SetType(type);

	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "Input idx is negative : " << idx << std::endl;
		return;
	}

	if (idx >= MAX_OBJ_NUM)
	{
		std::cout << "Input idx is negative : " << idx << std::endl;
		return;
	}

	if (m_obj[idx] == NULL)
	{
		std::cout << "m_obj[idx] is NULL" << idx << std::endl;
		return;
	}

	delete m_obj[idx];
	m_obj[idx] = NULL;
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = true;
	}
	if (key == 'a' || key == 'A')
	{
		m_KeyA = true;
	}
	if (key == 's' || key == 'S')
	{
		m_KeyS = true;
	}
	if (key == 'd' || key == 'D')
	{
		m_KeyD = true;
	}
	if (key == ' ')
	{
		m_KeySP = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_KeyA = false;
	}
	if (key == 's' || key == 'S')
	{
		m_KeyS = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_KeyD = false;
	}
	if (key == ' ')
	{
		m_KeySP = false;
	}
}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = false;
	}
}

void ScnMgr::Update(float elapsedInsec)
{
	//Character control : Hero
	//std::cout << "W:" << m_KeyW << ", A:" << m_KeyA << ", S:" << m_KeyS << ", D:" << m_KeyD << std::endl;
	std::cout << "Up:" << m_KeyUp << ", Down:" << m_KeyDown << ", Left:" << m_KeyLeft << ", Right:" << m_KeyRight << std::endl;

	float fx, fy, fz;
	fx = fy = fz = 0.f;
	float fAmount = 20.f;

	if (m_KeyW)
	{
		fy += 1.f;
	}
	if (m_KeyA)
	{
		fx -= 1.f;
	}
	if (m_KeyS)
	{
		fy -= 1.f;
	}
	if (m_KeyD)
	{
		fx += 1.f;
	}
	if (m_KeySP)
	{
		fz += 1.f;
	}
	
	//Add control force to Hero
	float fsize = sqrtf(fx * fx + fy * fy);

	if (fsize > FLT_EPSILON)
	{
		fx /= fsize;
		fy /= fsize;
		fx *= fAmount;
		fy *= fAmount;

		m_obj[HERO_ID]->AddForce(fx, fy, 0.f, elapsedInsec);
	}
	if (fz > FLT_EPSILON)
	{
		float x, y, z;
		m_obj[HERO_ID]->GetPosition(&x, &y, &z);

		if (z < FLT_EPSILON)
		{
			fz *= fAmount * 20.f;
			m_obj[HERO_ID]->AddForce(0.f, 0.f, fz, elapsedInsec);
		}
	}
	//Fire bullets
	if (m_obj[HERO_ID]->CanShootBullet())
	{
		float bulletVel = 5.f;
		float vBulletX, vBulletY, vBulletZ;

		vBulletX = vBulletY = vBulletZ = 0.f;

		if (m_KeyUp)	vBulletY += 1.f;
		if (m_KeyDown)	vBulletY -= 1.f;
		if (m_KeyLeft)	vBulletX -= 1.f;
		if (m_KeyRight)	vBulletX += 1.f;

		float vBulletSize = sqrtf(vBulletX * vBulletX + vBulletY * vBulletY + vBulletZ * vBulletZ);

		if (vBulletSize > FLT_EPSILON)
		{
			//Create bullet object
			vBulletX /= vBulletSize;
			vBulletY /= vBulletSize;
			vBulletZ /= vBulletSize;
			vBulletX *= bulletVel;
			vBulletY *= bulletVel;
			vBulletZ *= bulletVel;

			float hx, hy, hz;
			float hvx, hvy, hvz;

			m_obj[HERO_ID]->GetPosition(&hx, &hy, &hz);
			m_obj[HERO_ID]->GetVelocity(&hvx, &hvy, &hvz);

			vBulletX += hvx;
			vBulletY += hvy;
			vBulletZ += hvz;

			int temp = AddObject(
				hx, hy, hz,
				0.05f, 0.05f, 0.05f,
				0.f, 0.f, 1.f, 1.f,
				vBulletX, vBulletY, vBulletZ,
				1.f,
				0.7f,
				2.0f,
				TYPE_BULLET);

			m_obj[temp]->SetParentObj(m_obj[HERO_ID]);
			m_obj[HERO_ID]->ResetBulletCoolTime();
			//m_sound->PlayShortSound(g_FIRE, false, 1.f);
		}
	}

	// 전체 오브젝트에 대해서 overlapTest
	for (int src = 0; src < MAX_OBJ_NUM; ++src)
	{
		for (int trg = src + 1; trg < MAX_OBJ_NUM; ++trg) {
			if (m_obj[src] != NULL && m_obj[trg] != NULL) {
				// overlapp test
				if (m_physics->IsOverlap(m_obj[src], m_obj[trg])) {
					cout << "collision " << src << ", " << trg << "\n";

					// collision process
					if (!m_obj[src]->IsAncestor(m_obj[trg]) && !m_obj[trg]->IsAncestor(m_obj[src])) {
						m_physics->ProcessCollision(m_obj[src], m_obj[trg]);

						//Damage
						float srcHP, trgHP;
						m_obj[src]->GetHP(&srcHP);
						m_obj[trg]->GetHP(&trgHP);
						m_obj[src]->Damage(trgHP);
						m_obj[trg]->Damage(srcHP);

						//m_sound->PlayShortSound(g_EXPL, false, 1.f);
					}
				}
			}
		}
	}

	//For all objs, call update func
	for (int i = 0; i < MAX_OBJ_NUM; i++)
	{
		if (m_obj[i] != NULL)
		{
			m_obj[i]->Update(elapsedInsec);
		}
	}

	float x, y, z;
	m_obj[HERO_ID]->GetPosition(&x, &y, &z);
	m_Renderer->SetCameraPos(x * 100, y * 100);
}