#include "stdafx.h"
#include "Physics.h"

Physics::Physics()
{
}

Physics::~Physics()
{
}

bool Physics::IsOverlap(Object* A, Object* B, int type)
{
	switch (type)
	{
	case 0:
		// bboverlap
		return BBOverlapTest(A, B);
		break;
	case 1:
		break;

	}
}

void Physics::ProcessCollision(Object* A, Object* B)
{
	float aMass, aVX, aVY, aVZ;
	A->GetMass(&aMass);
	A->GetVelocity(&aVX, &aVY, &aVZ);
	float bMass, bVX, bVY, bVZ;
	B->GetMass(&bMass);
	B->GetVelocity(&bVX, &bVY, &bVZ);

	// final vel
	float afVX, afVY, afVZ;
	float bfVX, bfVY, bfVZ;

	afVX = ((aMass - bMass) / (aMass + bMass)) * aVX
		+ ((2.f * bMass) / (aMass + bMass)) * bVX;
	afVY = ((aMass - bMass) / (aMass + bMass)) * aVY
		+ ((2.f * bMass) / (aMass + bMass)) * bVY;
	afVZ = ((aMass - bMass) / (aMass + bMass)) * aVZ
		+ ((2.f * bMass) / (aMass + bMass)) * bVZ;

	bfVX = ((2.f * aMass) / (aMass + bMass)) * aVX
		+ ((bMass - aMass) / (aMass + bMass)) * bVX;
	bfVY = ((2.f * aMass) / (aMass + bMass)) * aVY
		+ ((bMass - aMass) / (aMass + bMass)) * bVY;
	bfVZ = ((2.f * aMass) / (aMass + bMass)) * aVZ
		+ ((bMass - aMass) / (aMass + bMass)) * bVZ;


	A->SetVelocity(afVX, afVY, afVZ);
	B->SetVelocity(bfVX, bfVY, bfVZ);
}

bool Physics::BBOverlapTest(Object* A, Object* B)
{
	float aX, aY, aZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	float aSX, aSY, aSZ;

	float bX, bY, bZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	float bSX, bSY, bSZ;

	// calc box A
	A->GetPosition(&aX, &aY, &aZ);
	A->GetVolume(&aSX, &aSY, &aSZ);
	aMinX = aX - aSX / 2.f;		aMaxX = aX + aSX / 2.f;
	aMinY = aY - aSY / 2.f;		aMaxY = aY + aSY / 2.f;
	aMinZ = aZ - aSZ / 2.f;		aMaxZ = aZ + aSZ / 2.f;

	// calc box B
	B->GetPosition(&bX, &bY, &bZ);
	B->GetVolume(&bSX, &bSY, &bSZ);
	bMinX = bX - bSX / 2.f;		bMaxX = bX + bSX / 2.f;
	bMinY = bY - bSY / 2.f;		bMaxY = bY + bSY / 2.f;
	bMinZ = bZ - bSZ / 2.f;		bMaxZ = bZ + bSZ / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;
	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;
	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;
}
