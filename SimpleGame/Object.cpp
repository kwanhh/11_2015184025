#include "stdafx.h"
#include "Object.h"
#include "Globals.h"

#include <cmath>
#include <cfloat>

Object::Object()
{
	InitPhysics();
}

Object::~Object()
{
}

void Object::Update(float elapsedInsec)
{
	// reduce bullet cool time
	m_remainingCoolTime -= elapsedInsec;


	//-------------------------Apply friction--------------------------
	float nForce = m_mass * GRAVITY;	//scalar
	float fForce = m_fricCoef * nForce;	//scalar

	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ * m_velZ);

	if (velSize > 0.f)
	{
		float fDirX = -1.f * m_velX / velSize;
		float fDirY = -1.f * m_velY / velSize;
		fDirX = fDirX * fForce;
		fDirY = fDirY * fForce;
		float fAccX = fDirX / m_mass;
		float fAccY = fDirY / m_mass;
		float newX = m_velX + fAccX * elapsedInsec;
		float newY = m_velY + fAccY * elapsedInsec;
		if (newX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newX;
		}
		if (newY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = newY;
		}
		m_velZ = m_velZ - GRAVITY * elapsedInsec;
	}
	//-----------------------------------------------------------------

	m_posX = m_posX + m_velX * elapsedInsec;
	m_posY = m_posY + m_velY * elapsedInsec;
	m_posZ = m_posZ + m_velZ * elapsedInsec;

	if (m_posZ < FLT_EPSILON)
	{
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void Object::AddForce(float x, float y, float z, float elapsedTime)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * elapsedTime;
	m_velY = m_velY + accY * elapsedTime;
	m_velZ = m_velZ + accZ * elapsedTime;
}

void Object::InitPhysics()
{
	m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f; //position
	m_mass = 0.f;                             //mass
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f; //velocity
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f; //acceleration
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f; //volume
	m_fricCoef = 0.f;
	m_type = -1;
}

void Object::SetSize(float sx, float sy, float sz)
{
	m_sx = sx;
	m_sy = sy;
	m_sz = sz;
}

void Object::GetSize(float* sx, float* sy, float* sz)
{
	*sx = m_sx;
	*sy = m_sy;
	*sz = m_sz;
}

void Object::SetLocation(float x, float y, float z)
{
	m_x = x;
	m_y = y;
	m_z = z;
}

void Object::GetLocation(float* x, float* y, float* z)
{
	*x = m_x;
	*y = m_y;
	*z = m_z;
}

void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}

void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::SetPosition(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}

void Object::GetPosition(float* x, float* y, float* z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::GetMass(float* mass)
{
	*mass = m_mass;
}

void Object::SetVelocity(float x, float y, float z)
{
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}

void Object::GetVelocity(float* x, float* y, float* z)
{
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}

void Object::SetAcceleration(float x, float y, float z)
{
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}

void Object::GetAcceleration(float* x, float* y, float* z)
{
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}

void Object::SetVolume(float x, float y, float z)
{
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}

void Object::GetVolume(float* x, float* y, float* z)
{
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}

void Object::SetFricCoef(float coef)
{
	m_fricCoef = coef;
}

void Object::GetFricCoef(float* coef)
{
	*coef = m_fricCoef;
}

void Object::SetType(int type)
{
	m_type = type;
}

void Object::GetType(int* type)
{
	*type = m_type;
}


bool Object::CanShootBullet()
{
	if (m_remainingCoolTime < FLT_EPSILON)
	{
		return true;
	}

	return false;
}

void Object::ResetBulletCoolTime()
{
	m_remainingCoolTime = m_currentCoolTime;
}

void Object::SetTexture(int id)
{
	m_texID = id;
}

void Object::GetTexture(int* id)
{
	*id = m_texID;
}

void Object::SetParentObj(Object* parent)
{
	m_parent = parent;
}

void Object::GetHP(float* hp)
{
	*hp = m_healthPoint;
}

void Object::SetHp(float hp)
{
	m_healthPoint = hp;
}

void Object::Damage(float damage)
{
	m_healthPoint = m_healthPoint - damage;
}

Object* Object::GetParentObj()
{
	return m_parent;
}

bool Object::IsAncestor(Object* obj)
{
	if (obj != NULL)
	{
		if (obj == m_parent)
			return true;
	}

	return false;
}

